# 2 - Architecture and Principles

* Internet under growing pains
  * IPV4 address exhaustion
  * Congestion Control
  * Routing - BGP is insecure, easily misconfigurable, and nondeterministic
  * Security
  * Denial of Service

Fundamental design goal of the Internet:

> Multiplexed utilization of existing interconnected networks.

* Packet Switching

  * Include information for forwarding traffic in the destination address of every packet.
  * Best effort level of service
  * Variable delay
  * No busy signal
  * Shares network resources
  * Contrast to circuit switching with dedicated line between sender and receiver (e.g. phone network)

* Narrow Waist
  * Larger layers above and below the network layer
  * Network layer has a single protocol in use: IP
  * Everything uses IP, so it's hard to change

Goals

* Survivability - work even if some devices fail
  * Design for Fate Sharing
  * If something goes down, it's okay to lose the state it held
* Heterogeneity - Support diversity
  * Protocols other than reliable TCP supported
  * Good for online video, where reliability not as important
* Distributed Management
  * Routing registries is not controlled by one organization
  * DNS for naming, BGP for routing policy
  * But, no owner
* Cost
* Ease of attachment
  * Anything with an IP stack can connect to the Internet
* Accountability
  * Ability to bill on usage, but wasn't prioritized

Missing Goals

* Security
* Availability
* Mobility
* Scaling

End-to-End Argument

* Argument for putting the logic at the endpoints of the connection rather than in the network
* The network should be dumb and minimal
* Innovation in apps and services
* Example: file transfer. Do checksum at both ends and retry if failure.
  * Easier than building in redundancy
* Violations
  * VPN Tunnels
  * TCP Splitting
    * Done on wireless networks for performance, override TCP congestion reaction
  * NAT
    * Rewrites packet to incoming port mapped to internal-only IP address

# 3 - Switching

* Hosts can send a datagram on a LAN with a MAC address

## ARP - Address Resolution Protocol

* Protocol to get MAC address given IP address
* Broadcast the request w/ IP to all hosts
* Response is unicast with the answer
* Host keeps ARP table mapping IP to MAC
* Encapsulation
  * With the MAC from ARP, takes IP packet and encapsulates in an Ethernet frame with destination IP address.

## Hubs

* Hub - simplest form of interconnection
* Hubs broadcast all frames they receive on an incoming port to all outgoing ports
* Flooding and collision, introducing latency

## Switches

* Hub + traffic isolation
* Partitions LAN into separate LAN segments
* Learning switches
  * Maintains a table between destination addresses and output ports on the switch
  * Uses source field of frames to populate the table
  * Must still flood when no entry in the table and for broadcast frames (e.g. ARP queries)
* Spanning trees
  * Because of flooding, must be careful of loops in the topology
  * Only flood along ports that are part of the spanning tree
  * Elect a root that is the switch with the smallest ID
  * Exclude links if not on the shortest path to the root

## Switches vs Routers

* Switches operate at Layer 2 (e.g. Ethernet)
* Routers operate at Layer 3 (IP)

## Buffer sizing in routers/switches

* `2 * T` is round-trip propagation delay. T time of units second
* `C` is the capacity to the bottleneck link (e.g. bits per second)
* `2T * C` - represents the maximum amount of outstanding data that could be on this path between the source and destination at any time
* If there are a large # of TCP flows, they will not be synchronized
  * Peaks are at different times
* This reduced the buffering requirement
* Using the Central Limit Theorem, the amount of buffer can be reduced to `2T*C / sqrt(n)` where `n` is the number of flows through the router

# 4 - Routing

* The Internet is not a single network, rather a collection of independent networks.
* These independent networks are called Autonomous Systems (AS).
* AS consists of nodes and edges. Nodes = Points of Presence (PoPs)
* Edges are fiber, typically along highways.
* GATech connects to the Atlanta PoP of the Abilene Network

## Intra-Domain Routing

### Distance-Vector Routing

* Each node sends multiple distance vectors to each of its neighbors
  * These are essentially copies of its own routing table
* Routers compute costs to each destination based on shortest path Bellman-Ford algorithm
* When vectors are received, local routing table is updated with shortest paths
* D-V Routing converges quickly when costs decrease, but bad news (failures) travels slowly
* Count-to-infinity problem - takes number of iterations to realize path has become long
* Solution is called "poison reverse" - advertise infinite cost if routing through destination
* Example of D-V Routing is the Routing Information Protocol (RIP)
  * Infinity set to 16
  * Send table refreshes every 30 seconds
  * Takes minutes to stabilize when router fails

### Link-State Routing

* Method used in most networks today
* Distribute a network map to every other node in the network
* Add costs of immediate neighbors, floods to neighboars
* Uses Dijkstra's shortest path algorithm
* O(n^3) complexity
* Cope with scale by introducing hierarchy among routers

## Interdomain routing

### BGP - Border Gateway Protocol

* Advertises routes between ASes
  * Components: Destination | Next Hop | AS Path
* EBGP - External BGP - routing information between border routers of adjacent ASes
* IBGP - Internal BGP - routing information about external destinations to routers inside AS
* IGP - Interior Gateway protocol - routing information about internal, to internal
* Route Selection
  1.  Prefer higher "local preference" - locally set by admin to set preferred routes
  2.  Shortest AS path length
  3.  Multi-exit discriminator (MED) - set preferred path out of an AS
  4.  Shortest IGP path (hot potato routing) - leave own network as soon as possible
  5.  Tiebreaker - most stable, lowest router ID
* BGP also has concept of "community" - simply a tag on a route

### Business models

* Prefer customer routes, because customer pays
* Then, peering because it's free
* Finally, use provider routes that are paid
* Export customer routes, but not provider routes because that costs you
* Routing can oscillate because of changing route priorities

# 5 - Naming, Addressing, & Forwarding

* IPv4 Address - 32 bit number split into 4 8-bit quads
* Pre-1994 - classful addressing
  * Class A: first bit 0, 7 bits for the network ID
  * Class B: first bits 10, next 14 bits for the network ID
  * Class C: first bits 110, next 21 bits for the network ID
  * Routing table growth explosion led to CIDR instead
* Classless Interdomain Routing (CIDR)
  * 32 bits: IP address + "mask"
  * Examples:
    * 192.168.0.0/16 has 2^16 addresses
    * 192.168.1.0/8 has 2^8 addresses
  * mask indicates length of the network ID prefix
  * Possible to have overlapping prefixes
* Use longest prefix match when looking up IP in routing table
  * Allows aggregaton of overlapped prefixes
* CIDR addressing slowed routing table growth, but was frustrated by multihoming
* Multihoming - reachable via multiple upstream ISPs
  * AS advertises prefix to both ISPs
  * Both have to advertise small /range, because aggregation will cause traffic to all come through one ISP link
  * Can deaggregate on purpose for load balancing

## Longest Prefix Match lookups

* Use Tries, hashtables are too memory inefficient
* Single bit (direct) tries are also too inefficient
* Multi-bit (Multi-ary) Trie - multiple branches off each node (not just 2)
* Leaf-pushed trie - instead of pointers, put entries in "sides" of the node

## Solutions for IPv4 Exhaustion

### NAT

* Allows multiple networks to reuse the same private IP space
* Translates global IP address to private (e.g. 192.186/16)
* Uses source port of packet to map to specific private IP
* Outgoing/Incoming packets are rewritten at the router
* Violates end-to-end model
* NAT failure results in unreachability
* Difficult for global host to reach private IP

### IPv6

* Just add more bits - 128 of them
  * 48 bits - Public routing topology
    * 03 bits - aggregation
    * 13 bits - top level provider
    * 08 bits - reserved
    * 24 bits - Even more bits
  * 16 bits - site identifier
  * 64 bit - interface id
* Hard to deploy incrementally - this is the narrow waist
  * Can do dual stack - communicate on 4 and 6
  * Tunnel 6 traffic inside a 4 packet

# 5.1 - Router Design Basics

Basic Router Architecture:

1.  Receive packet
2.  Determine destination from header
3.  Determine output interface from forwarding table
4.  Modifies header (e.g. TTL)
5.  Send packet to output interface

Line card - interface by which router sends/receives

* Each line card has copy of the forwarding table
  * Prevents lookup bottleneck
* Instead of shared bus for line card interconnect, use crossbar switching
  * Every input has connection to every output port
  * During each timeslice, each connected to 0 or 1 output
* Have virtual queue per output port to prevent single port blocking
* Scheduling & Fairness are both important in crossbar switching
* **Max-Min Fairness**
  * allocate small amounts all they want, leave the rest to high demanders
  * Examples:
    * { 2, 2.6, 4, 5} with capacity 10
    * Allocation: { 2, 2.6, 2.7, 2.7 }
    * { 1, 2, 5, 10 } with capacity 20
    * Allocation: { 1, 2, 5, 12 }
* Scheduling for Max-Min fairness?
  * Roundrobin is not fair, largest packet takes up more time
  * Bit by bit is completely fair, but unfeasible
  * Fair queueing - service packets according to soonest "finishing time"

# 5.2 - DNS

* Map domain to IP, but it can be slow. Solution: caching
* Hierarchical, but typically just worry about the result

Record Types

* A - Name -> IP Address
* NS - Name -> Nameserver (referral)
* MX - Name -> mail server
* CNAME - Canonical name
* PTR - IP -> Name (reverse lookup)
* AAAA - Name -> IPv6 address

Lookups

* Lookups happen in reverse hierarchy order. E.g. edu -> gatech
* For reverse lookups, start with `in-addr.arpa`
* For IP address 130.207.97.11, domain name lookup is 11.97.207.130.in-addr.arpa
* This allows the hierarchy traversal backwards
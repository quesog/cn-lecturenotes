# Architecture & Principles

1.  How does the “narrow waist” allow a large variety of different protocols to be used on the Internet?
1.  Discuss how the design principles of the Internet are manifest in the ARP protocol. Also discuss how the absence of some points from the design principles (e.g., security and mobility) has influenced ARP.
1.  What are the advantages of using circuit-switched networks instead of packet-switched networks (like the Internet) for streaming video?
1.  In what way are caching HTTP proxies a violation of the end-to-end argument? Also, what are the consequences of this?
1.  Why is it important for encryption to be end-to-end? In other words, what are the consequences if we perform encryption, but not in an end-to-end manner?
    a. (Bonus question: Gmail uses HTTPS to secure communication between your browser and the Gmail servers. Is this encryption “end-to-end”?)
1.  The forwarding tables for all switches in this network are initially empty. Host 192.168.1.1 sends and ARP request to discover the MAC address of the host with IP
    192.168.1.2. What entries will be in the forwarding table of each switch after host 192.168.1.1 receives the ARP reply?

    ![](images/review1/01.png)

1.  When a host on the private network side of a NAT with IP 192.168.1.100 opens a TCP connection to a web server at 123.45.67.89, what entry will be added to the following NAT table, and what will be the source and destination IP and port on the packet that the NAT router forwards on the WAN (public) side? (Recall that the default port for HTTP is port 80.)

    ![](images/review1/02.png)

1.  How does the Spanning Tree Protocol (STP) improve the reliability of Ethernets?
1.  Explain why 2T\*C represents the number of outstanding bits (or bits “in flight”) for a TCP flow. (Note: another way to write this would be RTT\*C )
1.  Suppose a particular router normally supports 1000 flows at a time. The average round-trip time (RTT) of each flow is 63ms, and the bottleneck link is 5 Mbps. According to the new thinking on buffer sizes (i.e., Appenzeller 2004), what size should the buffer on this router be?

# Reading - DARPA Internet Protocols

1.  Discuss the advantages of the fate-sharing survivability model present in Internet Protocols today, and how this model differs from a distributed state/replicated survivability model.
1.  Explain how the Internet architecture achieves the flexibility required to support a wide variety of networks? What functions is the network assumed to provide, and more importantly, what functions of the network are NOT assumed?
1.  How has the ARPANET design goal of distributed resource management across the network not been met in today’s Internet?
1.  Describe two of the design advantages of datagrams as the basic architectural feature of the Internet.

# Reading - End to End Arguments

1.  Consider a networked application that is latency sensitive and requires guaranteed delivery and of data packets between network hosts. Assume that errors in transmission
    are very rare. Would a network that violates the end-to-end principle by implementing packet acknowledgements and checksums within the low-level network architecture be a suitable choice for this application?
1.  Describe how the end-to-end argument supports clear definitions between network layers? ( i.e. why is reliable, in-order delivery packets a feature of a transport layer protocol and not the internet layer? Why is IP a connectionless protocol?)

IP Addressing and Forwarding

1.  Consider a subnet that has the following IP addresses. What is the smallest subnet (i.e., subnet with the longest prefix) that could include all these addresses? (Use CIDR, i.e. “slash”, notation.)
    * 192.168.165.1
    * 192.168.165.96
    * 192.168.160.1
    * 192.168.179.145
1.  How many addresses are in the subnet 10.11.12.128/26 ? (Show your work.)
1.  Now consider a router with the following forwarding table:

    ![](images/review1/03.png)

    1.  To which router will a datagram with destination IP address 73.214.106.198 be sent next?
    2.  To which router will a datagram with destination IP address 73.214.42.64 be sent next?
    3.  To which router will a datagram with destination IP address 73.214.39.216 be sent next?

# Routing

1.  Consider this network and the Distance Vector algorithm.

    ![](images/review1/04.png)

    What does router D update its distance vector to?

    ![](images/review1/05.png)

1.  Suppose you are operating an enterprise network (such at Georgia Tech’s network, or a medium-sized corporation’s network) and you decide to use RIP for your intra-AS routing. List two advantages that using RIP gives you (in contrast to using OSPF or IS-IS). 22. List two reasons for using Autonomous Systems (AS) in the Internet.
1.  BGP provides three ways for network operators to enforce policies on their networks. List these ways.

# Other Network Layer Protocols

1.  In an Internet where some routers are IPv6 routers (i.e., they support both IPv6 and
    IPv4) but most are IPv4 routers (that support only IPv4), how can two hosts
    communicate with each other using IPv6? (In other words, how can IPv6 be used if not
    all the routers are IPv6 routers?)
1.  Explain the ways in which DNS (the Domain Name System) achieves fault-tolerance, i.e.,
    such that most or all domain names can be resolved to IP addresses even if some (but
    not all) DNS servers crash.
1.  Consider a NAT router with the following entry in its translation table:

    ![](images/review1/06.png)

    1.  What does the NAT router do when it receives a packet on the WAN (Internet) interface with the destination address/port 138.76.29.7:5001?
    1.  What does the NAT router do when it receives a packet on the LAN (Local Area Network) interface with the source address/port 10.0.0.1:3345?
    1.  How does this entry get added to the NAT router’s translation table? (Assume the client is on the LAN side and the server is on the WAN side.)

1.  What is the difficulty with hosting a server (e.g. a web server) behind a NAT router (i.e.
    on the LAN side)?
1.  What is the difficulty with running a peer-to-peer application behind a NAT router?

# Reading - BGP Routing Policy in ISP Networks

1.  Why is BGP considered an incremental protocol?
1.  What does it mean for an attribute in the BGP decision process to be set by policy? Give
    an example of how a route can be determined by policy.
1.  Describe how an AS can set import filtering policies to protect itself from falsified BGP
    updates.

# Router Design Basics

1.  Describe the basic functions any router on the Internet must be able to perform in order
    to route traffic. What are some of the hardware features present on modern routers
    that support these functions?
1.  Describe the purpose of a Maximal Matching switching algorithm commonly used to
    schedule traffic on router crossbars.
1.  How would a router schedule bandwidth for a single timeslot (1s) for the following
    forwarding demands using a Max-Min Fairness algorithm (in Mb): 1.8, 2.4, 3.0, 5.3, 6.4,
    1.0, 2.2, 8.0? Assume the algorithm can forward at most 25.6 Mb of data in a single
    timeslot.

# Reading – Are we there yet?

1.  Why has deployment of IPv6 been slow to gain momentum despite the exhaustion of
    IPv4 address space?

# 6 - Congestion Control & Streaming

# Congestion Control

- "Fill the bucket as quickly as possible without overflowing"
- Increase in load -> Decrease in useful work
    - Spurious retransmission
    - Undelivered packets
- TCP Congestion control
    - Increase send rate until packet drops seen
- AIMD - Additive Increase, Multiplicative Decrease
    - Window based - *x* packets in flight at a time
    - Increase window every time ACK
- TCP Incast problem
    - Data center switch, too small buffers
    - Buffer fills, packet is dropped
    - Much more latency than necessary before resending packet
    - Solutions:
        - Smaller granularity times (microseconds vs milliseconds)
        - Fewer ACKs (every other)

## Streaming

- Large volume of data, low tolerance for variation in delay
- But, some loss is OK
- Use UDP instead of TCP
- QoS solutions for streaming
    - Marking packets as higher priority
    - Schedule packet queue to be more frequently processed